# multiredis-gitlab

Helm 3.0 chart to deploy multiple Redis for GitLab



## Usage

The defaults from `values.yaml` makes use of YAML anchors, to easy multiple configuration. If you need to change these settings, I suggest doing so.

```yaml
config: &config
  existingSecret: gitlab-redis-secret
  existingSecretPasswordKey: secret
  usePasswordFile: true
```

Installation for use:

```sh
helm install multiredis . -f custom.yaml
```

To consume this in GitLab (`gitlab/gitlab`)

```yaml
redis:
  install: false
global:
  redis:
    host: multiredis-app-master.default.svc
    sharedState:
      host: multiredis-sharedstate-master.default.svc
    actioncable:
      host: multiredis-actioncable-master.default.svc
    cache:
      host: multiredis-cache-master.default.svc
    queues:
      host: multiredis-queues-master.default.svc
```

```sh
helm install gitlab gitlab/gitlab -f gitlab.yaml -f multiredis.yaml
```